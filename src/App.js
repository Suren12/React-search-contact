import React, { Component } from 'react'
import './App.css'
import contacts from './date/contacts'
import Contact from './component/contact'

class App extends Component {
    state = {
            displayedContacts: contacts
    }
    handleSearch = (event) => {
        var searchQuery = event.target.value.toLowerCase();
        var displayedContacts = contacts.filter(function(el) {
            var searchValue = el.name.toLowerCase();
            return searchValue.indexOf(searchQuery) !== -1;
        })
        this.setState({
            displayedContacts: displayedContacts
        })
    }
    render() {
        return (
        <div className="App">
            <div className="contacts">
                <input type="text" placeholder="Search..." className="search-field" onChange={this.handleSearch} />
                <ul className="contacts-list">
                    {
                     this.state.displayedContacts.map((el) => <Contact
                                 key={el.id}
                                 name={el.name}
                                 phoneNumber={el.phoneNumber}
                                 image={el.image} />
                        )
                    }
                </ul>
            </div>
        </div>
        )
    }
}

export default App;
