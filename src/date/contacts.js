export default[
    {
        id: 1,
        name: 'Darth Vader',
        phoneNumber: '+374 0000000000001',
        image: 'img/darth.gif'
    }, {
        id: 2,
        name: 'Princess Leia',
        phoneNumber: '+374 0000000000002',
        image: 'img/leia.gif'
    }, {
        id: 3,
        name: 'Luke Skywalker',
        phoneNumber: '+374 0000000000003',
        image: 'img/luke.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        phoneNumber: '+374 0000000000004',
        image: 'img/chewbacca.gif'
    }, {
        id: 5,
        name: 'John',
        phoneNumber: '+374 0000000000005',
        image: 'img/John.jpg'
    }, {
        id: 6,
        name: 'Hugo',
        phoneNumber: '+374 0000000000006',
        image: 'img/hugo.jpg'
    }, {
        id: 7,
        name: 'Jack',
        phoneNumber: '+374 0000000000007',
        image: 'img/Jack.jpg'
    }, {
        id: 8,
        name: 'kate',
        phoneNumber: '+374 0000000000008',
        image: 'img/kate.jpg'
    }
];
